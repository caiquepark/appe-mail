<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


	require './Libs/PHPMailer.php';
	require './Libs/Exception.php';
	require './Libs/OAuth.php';
	require './Libs/POP3.php';
	require './Libs/SMTP.php';


	class Mensagem {
		private $para = null;
		private $assunto = null;
		private $mensagem = null;
		public $status = array('codigo_status' => null, 'descricao_status' => '');


		public	function __get($atributo) {
			return $this->$atributo;
		}

		public function __set($atributo, $valor) {
			$this->$atributo = $valor;
		}

		public function mensagemValida() {
			 if (empty($this->para) || empty($this->assunto) || empty($this->mensagem)) {
				 return false;
			 }

			 return true;
		}
		


	}


		$mensagem = new Mensagem();
		$mensagem-> __set('para', $_POST['para']);
		$mensagem->__set('assunto', $_POST['assunto']);
		$mensagem->__set('mensagem', $_POST['mensagem']);
		if(!$mensagem->mensagemValida()) {
			echo 'Preencha todos os campos ';
			//header('Location: index.php'); 
		}



		

		$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->SMTPDebug = false;                      //Enable verbose debug output
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = 'fulaninho@gmail.com';                     //SMTP username
    $mail->Password   = 'suasenha123';                               //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = 587;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    //Recipients
    $mail->setFrom('caiquetube@gmail.com', 'g-mail caca');
    $mail->addAddress($mensagem->__get('para'));     //Add a recipient
    //$mail->addAddress('ellen@example.com');               //Name is optional
    //$mail->addReplyTo('info@example.com', 'Information');
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    //Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
   // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

    //Content
    $mail->isHTML(true);                                  //Set email format to HTML
    $mail->Subject = $mensagem->__get('assunto');
    $mail->Body  = $mensagem->__get('mensagem');
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
	$mensagem->status['codigo_status'] = 1;	
	$mensagem->status['descricao_status'] = 'E-mail enviado com sucesso';
	 
    
} catch (Exception $e) {

	$mensagem->status['codigo_status'] = 2;	
	$mensagem->status['descricao_status'] = 'Por favor preencher todos os campos. Erro:' . $mail->ErrorInfo; 
     
}

		

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<title>Document</title>
</head>
<body>
	
	<div class="container d-flex justify-content-center">
		<div class="row d-block">
			<div class="py-3 text-center">
				<img class="d-block mx-auto mb-2" src="logo.png" alt="" width="72" height="72">
				<h2>Send Mail</h2>
				<p class="lead">Seu app de envio de e-mails particular!</p>
			</div>

			<div class="row d-block">
				<div class="col-md-12">
					<?php if($mensagem->status['codigo_status'] == 1) { ?>

						<div class="container d-block">
							<h1 class="display-4 text-center text-success">Sucesso</h1>
							<p class="text-success"><?=$mensagem->status['descricao_status']?></p>
							<div class="d-flex justify-content-center">
							<a href="index.php" class="btn btn-success bt-lg  mt-5 text-white">Voltar</a>
							</div>
						</div>

					 <?php } ?>

					<?php if($mensagem->status['codigo_status'] == 2) { ?>
						
						<div class="container d-block">
							<h1 class="display-4 text-center text-danger">Erro</h1>
							<p class="text-danger "><?=$mensagem->status['descricao_status']?></p>
							<div class="d-flex justify-content-center">
							<a href="index.php" class="btn btn-info bt-lg  mt-5 text-white">Voltar</a>
							</div>
						</div>

					<?php } ?>
				
				</div>
			</div>

		</div>
	</div>


</body>
</html>
